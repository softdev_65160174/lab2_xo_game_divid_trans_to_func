/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package TTTV2;

import java.util.Scanner;

/**
 *
 * @author Nobpharat
 */
public class XO_V_Divid_Func {

    static char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'X';
    static int row, col;

    public static void main(String[] args) {
        while (true) {
            printWelcome();
            printTable();
            while (true) {
                printTurn();
                inputRowCol();
                printTable();
                if (isWin()) {
                    printWinner();
                    break;
                }
                if (isDraw()) {
                    printDraw();
                    break;
                }
                changePlayer();
            }
            if(!isAgain()){
                break;
            }
        }

    }

    private static void printWelcome() {
        System.out.println("Welcome to XO");
    }

    private static void printTable() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println("");
        }
    }


    private static void printTurn() {
        System.out.println(currentPlayer + " Turn");
    }

    private static void inputRowCol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col : ");
        while (true) {
            row = sc.nextInt();
            col = sc.nextInt();
            if (row > 0 && row < 4 && col > 0 && col < 4) {
                if (table[row - 1][col - 1] == '-') {
                    table[row - 1][col - 1] = currentPlayer;
                    return;
                }
            }
            System.out.print("Invalid input. Please enter row[1-3] col[1-3] : ");
        }
    }

    private static void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }

    private static void printWinner() {
        System.out.println(currentPlayer + " is a Winner!");
    }

    private static boolean isWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkDiagonal()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (table[row - 1][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (table[i][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDiagonal() {
        if ((table[0][0] == currentPlayer && table[0][0] == table[1][1] && table[1][1] == table[2][2])
                || (table[1][1] == currentPlayer && table[1][1] == table[0][2] && table[1][1] == table[2][0])) {
            return true;
        }
        return false;
    }

    private static boolean isDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[i][j] != 'X' && table[i][j] != 'O') {
                    return false;
                }
            }
        }
        return true;

    }

    private static void printDraw() {
        System.out.println("Game is a draw!");
    }

    private static boolean isAgain() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input continue (y/n)  : ");
        while (true) {
                String ans = sc.next();
                if (ans.equals("y") || ans.equals("n")) {
                    if(ans.equals("y")){
                        return true;
                    }else{
                        return false;
                    }
                }
                System.out.println("to start the game. Please choose y or n.");
            }
        }

    


}
